/*package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Set;

*//* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*//*

public class Solution
{
    public static HashSet<Integer> createSet()
    {
        //напишите тут ваш код

    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        //напишите тут ваш код

    }
}*/


package com.javarush.test.level08.lesson08.task02;

import java.util.HashSet;
import java.util.Set;

/* Удалить все числа больше 10
Создать множество чисел(Set<Integer>), занести туда 20 различных чисел.
Удалить из множества все числа больше 10.
*/

public class Solution
{

    public static void main(String[] args)
    {

        HashSet<Integer> k = createSet();
        k = removeAllNumbersMoreThan10(k);
        for (Integer l : k){

            System.out.println(l);

        }


    }

    public static HashSet<Integer> createSet()
    {
        HashSet<Integer> i = new HashSet<>();

        for(int j = 0; j < 20; j++){

            i.add(j*2);

        }

        return i;

    }

    public static HashSet<Integer> removeAllNumbersMoreThan10(HashSet<Integer> set)
    {
        HashSet<Integer> h = new HashSet<>();

        for (Integer g : set){

            if (g < 10){

                h.add(g);

            }
        }

        return h;

    }
}
