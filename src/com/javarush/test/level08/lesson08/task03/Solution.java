/*package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;

*//* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*//*

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //напишите тут ваш код

    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        //напишите тут ваш код

    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName)
    {
        //напишите тут ваш код

    }
}*/

package com.javarush.test.level08.lesson08.task03;

import java.util.HashMap;
import java.util.HashSet;

/* Одинаковые имя и фамилия
Создать словарь (Map<String, String>) занести в него десять записей по принципу «Фамилия» - «Имя».
Проверить сколько людей имеют совпадающие с заданным имя или фамилию.
*/

public class Solution
{

    public static void main(String[] args) {

        HashMap<String, String> listnew = createMap();
        System.out.println(getCountTheSameFirstName(listnew, "Андрей"));
        System.out.println(getCountTheSameLastName(listnew, "Васильев"));

    }

    public static HashMap<String, String> createMap()
    {
        HashMap<String, String> list = new HashMap<>();
        list.put("Васильев", "Андрей");
        list.put("Чапаев", "Андрей");
        list.put("Логинов", "Семен");
        list.put("Круглов", "Данил");
        list.put("Губарев", "Василий");
        list.put("Козачков", "Николай");
        list.put("Сеничкин", "Андрей");
        list.put("Петров", "Данил");
        list.put("Лермонтов", "Леонид");
        list.put("Крид", "Егор");
        return list;
    }

    public static int getCountTheSameFirstName(HashMap<String, String> map, String name)
    {
        int i = 0;
        for (String s : map.values())
        {
            if (s.equals(name)){
                i=i+1;
            }
        }
        return i;
    }

    public static int getCountTheSameLastName(HashMap<String, String> map, String lastName)
    {
        int j = 0;
        for (String s : map.keySet())
        {
            if (s.equals(lastName)){
                j=j+1;
            }
        }
        return j;
    }
}
