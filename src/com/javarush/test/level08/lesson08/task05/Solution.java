/*package com.javarush.test.level08.lesson08.task05;

import java.util.HashMap;
import java.util.Map;

*//* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*//*

public class Solution
{
    public static HashMap<String, String> createMap()
    {
        //напишите тут ваш код

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {
        //напишите тут ваш код

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }
}*/

package com.javarush.test.level08.lesson08.task05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* Удалить людей, имеющих одинаковые имена
Создать словарь (Map<String, String>) занести в него десять записей по принципу «фамилия» - «имя».
Удалить людей, имеющих одинаковые имена.
*/

public class Solution
{
    public static HashMap<String, String> createMap()
    {

        HashMap<String, String> map2 = new HashMap<>();
        map2.put("Козачков", "Николай");
        map2.put("Сеничкин", "Андрей");
        map2.put("Петров", "Данил");
        map2.put("Лермонтов", "Леонид");
        map2.put("Крид", "Егор");
        map2.put("Васильев", "Андрей");
        map2.put("Чапаев", "Андрей");
        map2.put("Логинов", "Семен");
        map2.put("Круглов", "Данил");
        map2.put("Губарев", "Василий");
        return map2;

    }

    public static void removeTheFirstNameDuplicates(HashMap<String, String> map)
    {

        String s;

        ArrayList<String> j = new ArrayList<>();
        for (Map.Entry<String, String> pair: map.entrySet())
        {
            int i = 0;
            s = pair.getValue();
            for (Map.Entry<String, String> pair2: map.entrySet())
            {
                if (pair2.getValue().equals(s))
                 i++;
            }
            if (i>1){
               j.add(s);
            }

        }
        for (String l : j){

            removeItemFromMapByValue(map, l);

        }

    }

    public static void removeItemFromMapByValue(HashMap<String, String> map, String value)
    {
        HashMap<String, String> copy = new HashMap<String, String>(map);
        for (Map.Entry<String, String> pair: copy.entrySet())
        {
            if (pair.getValue().equals(value))
                map.remove(pair.getKey());
        }
    }

}
