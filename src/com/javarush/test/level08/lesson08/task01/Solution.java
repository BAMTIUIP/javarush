/*package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

*//* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*//*

public class Solution
{
    public static HashSet<String> createSet()
    {
        //напишите тут ваш код

    }
}*/


package com.javarush.test.level08.lesson08.task01;

import java.util.HashSet;
import java.util.Set;

/* 20 слов на букву «Л»
Создать множество строк (Set<String>), занести в него 20 слов на букву «Л».
*/

public class Solution
{
    public static HashSet<String> createSet()
    {

        HashSet<String> words = new HashSet<>();

        words.add("Ложь");
        words.add("Ложка");
        words.add("Лес");
        words.add("Лор");
        words.add("Лёха");
        words.add("Лёхка");
        words.add("Лёхпа");
        words.add("Лёиха");
        words.add("Лёмха");
        words.add("Лёдха");
        words.add("Лёхаasd");
        words.add("Лёяха");
        words.add("Лсёха");
        words.add("Лёчсха");
        words.add("Лёмсха");
        words.add("Лёчсмха");
        words.add("Лёчясмха");
        words.add("Лёчсмчсмха");
        words.add("Лёячсмячсмха");
        words.add("Лёчясмячсмячсмячсха");

        return words;
    }
}
