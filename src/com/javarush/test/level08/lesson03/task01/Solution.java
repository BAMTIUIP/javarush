package com.javarush.test.level08.lesson03.task01;

/* HashSet из растений
Создать коллекцию HashSet с типом элементов String.
Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
Вывести содержимое коллекции на экран, каждый элемент с новой строки.
Посмотреть, как изменился порядок добавленных элементов.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        //напишите тут ваш код

    }
}


/*package com.javarush.test.level08.lesson03.task01;

*//* HashSet из растений
Создать коллекцию HashSet с типом элементов String.
Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
Вывести содержимое коллекции на экран, каждый элемент с новой строки.
Посмотреть, как изменился порядок добавленных элементов.
*//*

import java.util.HashSet;

public class Solution
{
    public static void main(String[] args) throws Exception
    {

        HashSet<String> natural = new HashSet<>();
        natural.add("арбуз");
        natural.add("банан");
        natural.add("вишня");
        natural.add("груша");
        natural.add("дыня");
        natural.add("ежевика");
        natural.add("жень-шень");
        natural.add("земляника");
        natural.add("ирис");
        natural.add("картофель");

        for(String x : natural){

            System.out.println(x);

        }

    }
}*/