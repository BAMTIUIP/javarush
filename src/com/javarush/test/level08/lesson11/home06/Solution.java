/*package com.javarush.test.level08.lesson11.home06;

*//* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*//*

public class Solution
{
    public static void main(String[] args)
    {
        //напишите тут ваш код
    }

    public static class Human
    {
        //напишите тут ваш код

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}*/

package com.javarush.test.level08.lesson11.home06;

/* Вся семья в сборе
1. Создай класс Human с полями имя (String), пол (boolean), возраст (int), дети (ArrayList<Human>).
2. Создай объекты и заполни их так, чтобы получилось: два дедушки, две бабушки, отец, мать, трое детей.
3. Вывести все объекты Human на экран.
*/

import java.util.ArrayList;

public class Solution
{
    public static void main(String[] args)
    {

        ArrayList<Human> all = new ArrayList<>();
        Human son1 = new Human("Вася", true, 7);
        Human son2 = new Human("Петя", true, 5);
        Human dother = new Human("Лена", false, 3);
        all.add(son1);
        all.add(son2);
        all.add(dother);
        ArrayList<Human> children1 = new ArrayList<>();
        children1.add(son1);
        children1.add(son2);
        children1.add(dother);
        Human father = new Human("Сергей", true, 35, children1);
        Human mother = new Human("Оксана", false, 33, children1);
        all.add(father);
        all.add(mother);
        ArrayList<Human> children2 = new ArrayList<>();
        children2.add(father);
        ArrayList<Human> children3 = new ArrayList<>();
        children3.add(mother);
        Human grandf1 = new Human("Алексей", true, 67, children2);
        Human grandf2 = new Human("Ахмед", true, 65, children3);
        Human grandm1 = new Human("Валя", false, 66, children2);
        Human grandm2 = new Human("Мария", false, 64, children3);
        all.add(grandf1);
        all.add(grandf2);
        all.add(grandm1);
        all.add(grandm2);
        for (Human x : all){
            System.out.println(x);
        }

    }

    public static class Human
    {
        String name;
        boolean sex;
        int age;
        ArrayList<Human> children= new ArrayList<>();

        public Human(String name, boolean sex, int age) {
            this.name = name;
            this.sex = sex;
            this.age = age;
        }

        public Human(String name, boolean sex, int age, ArrayList<Human> children) {
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.children = children;
        }

        public String toString()
        {
            String text = "";
            text += "Имя: " + this.name;
            text += ", пол: " + (this.sex ? "мужской" : "женский");
            text += ", возраст: " + this.age;

            int childCount = this.children.size();
            if (childCount > 0)
            {
                text += ", дети: "+this.children.get(0).name;

                for (int i = 1; i < childCount; i++)
                {
                    Human child = this.children.get(i);
                    text += ", "+child.name;
                }
            }

            return text;
        }
    }

}
