/*package com.javarush.test.level08.lesson11.home09;

import java.util.Date;

*//* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*//*

public class Solution
{
    public static void main(String[] args)
    {
    }

    public static boolean isDateOdd(String date)
    {
        return true;
    }
}*/


package com.javarush.test.level08.lesson11.home09;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/* Работа с датой
1. Реализовать метод isDateOdd(String date) так, чтобы он возвращал true, если количество дней с начала года - нечетное число, иначе false
2. String date передается в формате MAY 1 2013
Не забудьте учесть первый день года.
Пример:
JANUARY 1 2000 = true
JANUARY 2 2020 = false
*/

public class Solution
{
    public static void main(String[] args)
    {

        SimpleDateFormat format1 = new SimpleDateFormat("MMMM dd yyyy", Locale.ENGLISH);
        String s = format1.format(new Date());
        System.out.println(s + " = " + isDateOdd(s));

    }
    public static boolean isDateOdd(String date)
    {
        Date y = new Date(date);
        Date x = new Date();
        x.setDate(1);
        x.setMonth(0);
        x.setMinutes(0);
        x.setHours(0);
        x.setSeconds(0);
        long msTimeDistance = y.getTime() - x.getTime();
        long deltaInDays = (msTimeDistance /1000/60/60/24)+1;
        if (deltaInDays%2 == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
