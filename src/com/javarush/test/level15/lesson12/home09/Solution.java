package com.javarush.test.level15.lesson12.home09;

/* Парсер реквестов
Считать с консоли URl ссылку.
Вывести на экран через пробел список всех параметров (Параметры идут после ? и разделяются &, например, lvl=15).
URL содержит минимум 1 параметр.
Если присутствует параметр obj, то передать его значение в нужный метод alert.
alert(double value) - для чисел (дробные числа разделяются точкой)
alert(String value) - для строк

Пример 1
Ввод:
http://javarush.ru/alpha/index.html?lvl=15&view&name=Amigo
Вывод:
lvl view name

Пример 2
Ввод:
http://javarush.ru/alpha/index.html?obj=3.14&name=Amigo
Вывод:
obj name
double 3.14
*/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) throws IOException
    {
        //add your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String url = reader.readLine();
        String result = "";
        String parameter = "";
        ArrayList<String> objValue = new ArrayList<String>();
        url = url.substring(url.indexOf("?")+1,url.length());
        while(url.contains("?")){
                url = url.replaceAll("\\?","");
        }
        while(url.contains("&&")){
                url = url.replaceAll("&&","&");
        }
        while(url.contains("==")){
                url = url.replaceAll("==","=");
        }
        while (true)
        {

            if (url.indexOf("&") == -1)
            {

                parameter = url.substring(0,url.indexOf("="));
                result = result + (parameter.equals("") ? "" : parameter);
                if (parameter.equals("obj"))
                {
                    objValue.add(url.substring(url.indexOf("=") + 1, url.length()));
                }
                break;
            }

            parameter = url.substring(0,url.indexOf("&") < url.indexOf("=") ? url.indexOf("&") : url.indexOf("="));
            if (parameter.equals("obj"))
            {
                objValue.add(url.indexOf("&") < url.indexOf("=") ? "" : url.substring(url.indexOf("=")+1, url.indexOf("&")));
            }
            result = result + (parameter.equals("") ? "" : parameter + " ");
            url = url.substring(url.indexOf("&")+1,url.length());

        }

        System.out.println(result);

        for (String x : objValue)
        {
            if (!x.equals(""))
            {
                try
                {
                    alert((double) Double.parseDouble(x));
                }
                catch (Exception e)
                {
                    alert(x);
                }
            }
        }
    }

    public static void alert(double value) {
        System.out.println("double " + value);
    }

    public static void alert(String value) {
        System.out.println("String " + value);
    }
}
