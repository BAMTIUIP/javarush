package com.javarush.test.level10.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* Количество букв
Ввести с клавиатуры 10 строчек и подсчитать в них количество различных букв (для 33 букв алфавита).  Вывести результат на экран.
Пример вывода:
а 5
б 8
в 3
г 7
д 0
…
я 9
*/

public class Solution
{
    public static void main(String[] args)  throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (int i = 0; i < abcArray.length; i++)
        {
            alphabet.add(abcArray[i]);
        }
        //ввод строк
        ArrayList<String> list = new ArrayList<>();
        for(int i=0;i<10;i++)
        {
            String s = reader.readLine();
            list.add( s.toLowerCase());
        }

        //Введите здесь свой код
    }

}

/* Прошла

package com.javarush.test.level10.lesson11.home05;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

*//* Количество букв
Ввести с клавиатуры 10 строчек и подсчитать в них количество различных букв (для 33 букв алфавита).  Вывести результат на экран.
Пример вывода:
а 5
б 8
в 3
г 7
д 0
…
я 9
*//*

public class Solution
{
    public static void main(String[] args)  throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<Character>();
        for (int i = 0; i < abcArray.length; i++)
        {
            alphabet.add(abcArray[i]);
        }
        //ввод строк
        ArrayList<String> list = new ArrayList<>();
        for(int i=0;i<10;i++)
        {
            String s = reader.readLine();
            list.add( s.toLowerCase());
        }
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < 33; i++) map.put(alphabet.get(i),0);


        for (Map.Entry<Character, Integer> pair : map.entrySet()){
            for (String j : list)
                for (char k : j.toCharArray())
                    if (k == pair.getKey()) pair.setValue(pair.getValue()+1);

        }
        for (Character i : alphabet)
            System.out.println(i + " " + map.get(i));
    }

}*/


/* Не прошла
package com.javarush.test.level10.lesson11.home05;

        import java.io.BufferedReader;
        import java.io.InputStreamReader;
        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Map;

*/
/* Количество букв
Ввести с клавиатуры 10 строчек и подсчитать в них количество различных букв (для 33 букв алфавита).  Вывести результат на экран.
Пример вывода:
а 5
б 8
в 3
г 7
д 0
…
я 9
*//*


public class Solution
{
    public static void main(String[] args)  throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        //алфавит
        String abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        char[] abcArray = abc.toCharArray();

        ArrayList<Character> alphabet = new ArrayList<>();
        HashMap<Character, Integer> alphamap = new HashMap<>();
        for (int i = 0; i < abcArray.length; i++)
        {
            alphabet.add(abcArray[i]);
            alphamap.put(abcArray[i], 0);
        }

        //ввод строк
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < 10; i++)
        {
            String s = reader.readLine();
            list.add(s.toLowerCase());
        }
        ArrayList<Character> buffer = new ArrayList<Character>();
        for(int i = 0; i < list.size(); i ++){
            char [] myCharArray = list.get(i).toCharArray ();
            for (int j = 0; j < myCharArray.length; j++){
                buffer.add(myCharArray[j]);
            }
            for (Map.Entry<Character, Integer> pair : alphamap.entrySet()){
                Integer value = pair.getValue();
                Character key = pair.getKey();
                for(Character x:buffer){
                    if (key.equals(x)){
                        pair.setValue(value++);
                    }
                }
            }
            buffer.removeAll(buffer);
        }
        for(Character l:alphabet){
            for (Map.Entry<Character, Integer> pair : alphamap.entrySet()){
                Integer value = pair.getValue();
                Character key = pair.getKey();
                if (key.equals(l)){
                    System.out.println(key + " " + value);
                }
            }
        }
    }
}
*/



