package com.javarush.test.level10.lesson11.home06;

/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*/

public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        //напишите тут ваши переменные и конструкторы
    }
}


/*
package com.javarush.test.level10.lesson11.home06;

*/
/* Конструкторы класса Human
Напиши класс Human с 6 полями. Придумай и реализуй 10 различных конструкторов для него. Каждый конструктор должен иметь смысл.
*//*


public class Solution
{
    public static void main(String[] args)
    {

    }

    public static class Human
    {
        String name;
        int age;
        Boolean sex;
        int width;
        Human father;
        Human mother;

        public Human(String name) {
            this.name = name;
        }

        public Human(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Human(String name, Human father, Human mother) {
            this.name = name;
            this.father = father;
            this.mother = mother;
        }

        public Human(String name, Human father) {

            this.name = name;
            this.father = father;
        }

        public Human(String name, Human father, Human mother, int age) {
            this.name = name;
            this.father = father;
            this.mother = mother;
            this.age = age;
        }

        public Human(String name, Boolean sex) {
            this.name = name;
            this.sex = sex;
        }

        public Human(String name, int age, Boolean sex, int width) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.width = width;
        }

        public Human(String name, int age, Boolean sex, int width, Human father, Human mother) {
            this.name = name;
            this.age = age;
            this.sex = sex;
            this.width = width;
            this.father = father;
            this.mother = mother;
        }

        public Human(String name, int age, Boolean sex) {
            this.name = name;
            this.age = age;
            this.sex = sex;
        }

        public Human(String name, Boolean sex, int width) {
            this.name = name;
            this.sex = sex;
            this.width = width;
        }
    }
}
*/
