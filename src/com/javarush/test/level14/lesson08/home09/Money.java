package com.javarush.test.level14.lesson08.home09;

public abstract class Money
{
    private static double a;

    protected Money() {
    }

    public static double getAmount(){

        return Money.a;

    }

    public void setA(double a) {
        this.a = a;
    }

    public abstract String getCurrencyName();
}

