package com.javarush.test.level14.lesson08.home01;

/**
 * Created by SBT-Yanovskiy-ID on 01.02.2016.
 */
public interface Bridge {
    int getCarsCount();
}
