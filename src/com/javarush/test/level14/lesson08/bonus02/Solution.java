package com.javarush.test.level14.lesson08.bonus02;

/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*/

public class Solution
{
    public static void main(String[] args) throws Exception
    {
    }
}

/*

package com.javarush.test.level14.lesson08.bonus02;

*/
/* НОД
Наибольший общий делитель (НОД).
Ввести с клавиатуры 2 целых положительных числа.
Вывести в консоль наибольший общий делитель.
*//*


import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution
{
    public static void main(String[] args) throws Exception
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int i=Integer.parseInt(reader.readLine());
        int j=Integer.parseInt(reader.readLine());
        Boolean exit = true;
        int k = 1;
        int max=1;
        int min = i<j ? i:j;
        while(k<min){

            if ((i%k)==0 & (j%k)==0){
                max=k;
            }
            else{

            }
            k++;
        }
        System.out.println(max);

    }
}
*/
